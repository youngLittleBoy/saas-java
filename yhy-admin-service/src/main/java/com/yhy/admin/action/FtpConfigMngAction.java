package com.yhy.admin.action;

import com.yhy.admin.dto.FtpConfigDTO;
import com.yhy.admin.service.FtpConfigSetService;
import com.yhy.admin.service.mng.FtpConfigMngService;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b>FTP设置<br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午2:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/ftpConfigMng", produces="application/json;charset=UTF-8")
public class FtpConfigMngAction extends BaseMngAction<FtpConfigDTO> {

    @Autowired
    protected FtpConfigSetService ftpConfigSetService;

    @Override
    protected FtpConfigMngService getBaseService() {
        return SpringContextHolder.getBean(FtpConfigMngService.class);
    }

    /*
     * 获取ftp所有配置
     */
    @RequestMapping(value="/getAllFtpConfig",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取ftp配置", notes="获取ftp配置")
    @ApiImplicitParams({
    })
    public AppReturnMsg getAllFtpConfig(HttpServletRequest request, HttpServletResponse response) {
        return AppReturnMsg.success("",ftpConfigSetService.getAllFtpConfig(YhyUtils.getSysUser().getCpyCode()));
    }

    @Override
    protected void beforeOfSaveProcess(FtpConfigDTO baseDTO) {
        super.beforeOfSaveProcess(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FTPCONFIG_ALL","FTPCONFIG_CREATE","FTPCONFIG_CHANGE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FtpConfigDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody FtpConfigDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FTPCONFIG_ALL","FTPCONFIG_CREATE","FTPCONFIG_CHANGE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FtpConfigDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody FtpConfigDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FTPCONFIG_ALL","FTPCONFIG_CREATE","FTPCONFIG_CHANGE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass =FtpConfigDTO.class)
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody FtpConfigDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass =FtpConfigDTO.class)
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody FtpConfigDTO baseDTO) {
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FTPCONFIG_ALL","FTPCONFIG_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataTypeClass =FtpConfigDTO.class)
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody FtpConfigDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }


}
