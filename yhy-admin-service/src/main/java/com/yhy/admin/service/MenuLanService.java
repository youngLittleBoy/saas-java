package com.yhy.admin.service;

import com.yhy.admin.dao.MenuLanDao;
import com.yhy.admin.vo.MenuLanVO;
import com.yhy.common.dao.BaseDao;
import com.yhy.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-1 下午5:08 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class MenuLanService extends BaseService<MenuLanVO> {

    @Autowired
    private MenuLanDao menuLanDao;

    @Override
    protected MenuLanDao getDao() {
        return menuLanDao;
    }


    public List<MenuLanVO> findByMenuId(String menuId) {
        MenuLanVO param = new MenuLanVO();
        param.setMenuId(menuId);
        param.setEnableFlag("Y");
        return getDao().findBy(param);
    }

}
