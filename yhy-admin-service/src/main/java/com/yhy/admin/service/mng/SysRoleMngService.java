package com.yhy.admin.service.mng;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.yhy.admin.dao.SysRoleMngDao;
import com.yhy.admin.dto.SysRoleDTO;
import com.yhy.admin.service.*;
import com.yhy.admin.vo.*;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.admin.vo.mng.SysRoleMngVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.constants.BusPrivCode;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseMngService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-9 下午3:16 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:26 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleMngService extends BaseMngService<SysRoleDTO,SysRoleMngVO> {

    @Autowired
    private SysRoleMngDao sysRoleMngDao;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Autowired
    private SysRoleOrgService sysRoleOrgService;
    @Autowired
    private SysRoleFormService sysRoleFormService;
    @Autowired
    private SysRoleUserService sysRoleUserService;
    @Autowired
    private MenuService menuService;

    @Autowired
    private OrgSetService orgSetService;

    @Override
    protected SysRoleMngDao getBaseMngDao() {
        return sysRoleMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_SYS_ROLE_MNG.getVal();
    }

    @Override
    public SysRoleService getBaseMainService() {
        return sysRoleService;
    }


    public void autoCreateDefaultRole(String cpyCode, String orgsetId) {
        SysRoleDTO baseDto = new SysRoleDTO();
        SysRoleMngVO mngVO = new SysRoleMngVO();
        mngVO.setSysOwnerCpy(cpyCode);
        mngVO.setRoleName("默认管理员角色");
        mngVO.setRmk("系统自动生成");
        mngVO.setAttribute1(BusPrivCode.DFT_ADMIN_ROLE.getVal());
        mngVO.setEnableFlag("Y");
        baseDto.setBusMainData(mngVO);
        //1保存角色
        doSave(baseDto);

        //2保存角色与菜单关系
        List<SysRoleMenuVO> menuVOList = Lists.newArrayList();
        List<MenuVO> allMenus = menuService.findAllNotAdminMenu();
        for (MenuVO tmpVO : allMenus) {
            SysRoleMenuVO menuVO = new SysRoleMenuVO();
            menuVO.setMenuId(tmpVO.getId());
            menuVOList.add(menuVO);
        }
        baseDto.getBusMainData().setRoleMenuVOList(menuVOList);
        this.saveMenu(baseDto);

        //4保存角色与组织关系
        List<SysRoleOrgVO> orgVOList = Lists.newArrayList();
        OrgSetVO orgSetVO = orgSetService.findById(orgsetId);
        SysRoleOrgVO addVO = new SysRoleOrgVO();
        addVO.setOrgId(orgsetId);
        addVO.setOrgCode(orgSetVO.getOrgCode());
        orgVOList.add(addVO);
        baseDto.getBusMainData().setRoleOrgVOList(orgVOList);
        this.saveOrg(baseDto);
    }

    @Override
    protected void beforeDeleteOfProcess(SysRoleMngVO busMainData) {
        if(BusPrivCode.PRIV_SYS_ADMIN.getVal().equals(busMainData.getAttribute1()) || BusPrivCode.DFT_ADMIN_ROLE.getVal().equals(busMainData.getAttribute2())) {
            throw new BusinessException("系统默认管理员不能删除");
        }
    }

    @Override
    protected void afterOfProcessLoadData(SysRoleDTO baseDTO) {
        super.afterOfProcessLoadData(baseDTO);

        for (SysRoleMngVO mngVO : baseDTO.getRtnList()) {
            List<SysRoleMenuVO> roleMenuVOList = sysRoleMenuService.findByRoleId(mngVO.getId());
            mngVO.setRoleMenuVOList(roleMenuVOList);

            List<SysRoleOrgVO> roleOrgVOList = sysRoleOrgService.findByRoleId(mngVO.getId());
            mngVO.setRoleOrgVOList(roleOrgVOList);
        }
    }

    public AppReturnMsg saveMenu(SysRoleDTO baseDTO) {
        sysRoleMenuService.deleteByRoleId(baseDTO.getBusMainData().getId());
        for (SysRoleMenuVO tmpVO : baseDTO.getBusMainData().getRoleMenuVOList()) {
            tmpVO.setId(null);
            tmpVO.setEnableFlag("Y");
            tmpVO.setRoleId(baseDTO.getBusMainData().getId());
            sysRoleMenuService.saveOrUpdate(tmpVO);
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",baseDTO,null);
    }

    public AppReturnMsg saveOrg(SysRoleDTO baseDTO) {
        sysRoleOrgService.deleteByRoleId(baseDTO.getBusMainData().getId());
        for (SysRoleOrgVO tmpVO : baseDTO.getBusMainData().getRoleOrgVOList()) {
            tmpVO.setId(null);
            tmpVO.setEnableFlag("Y");
            tmpVO.setRoleId(baseDTO.getBusMainData().getId());
            sysRoleOrgService.saveOrUpdate(tmpVO);
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",baseDTO,null);
    }

    public AppReturnMsg saveRoleForm(SysRoleDTO baseDTO) {
        for (SysRoleFormVO tmpVO : baseDTO.getBusMainData().getRoleFormVOList()) {
            tmpVO.setId(null);
            tmpVO.setEnableFlag("Y");
            tmpVO.setRoleId(baseDTO.getBusMainData().getId());
            tmpVO.setFormCode(tmpVO.getFormCode());
            if(!sysRoleFormService.hasBy(tmpVO)) {
                sysRoleFormService.insert(tmpVO);
            }
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",baseDTO,null);
    }

    public AppReturnMsg saveUser(SysRoleDTO baseDTO) {
        /*if(!sysRoleUserService.hasBy(tmpVO)) {
            tmpVO.setEnableFlag("Y");
            tmpVO.setRoleId(baseDTO.getBusMainData().getId());
        }*/
        for (SysRoleUserVO tmpVO : baseDTO.getBusMainData().getRoleUserVOList()) {
            tmpVO.setId(null);
            tmpVO.setEnableFlag("Y");
            tmpVO.setRoleId(baseDTO.getBusMainData().getId());
            tmpVO.setUserId(tmpVO.getUserId());
            if(!sysRoleUserService.hasBy(tmpVO)) {
                sysRoleUserService.insert(tmpVO);
            }
        }
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",baseDTO,null);
    }


    public Set<String> findRoleByUserInfo(String sysOwnerCpy, String userAccount) {
        if(StringUtils.isAnyBlank(sysOwnerCpy,userAccount)) {
            return Sets.newHashSet();
        }
        Set<String> rtn = Sets.newHashSet();
        List<SysRoleMngVO> rtnList = getBaseMngDao().findRoleByUserInfo(sysOwnerCpy,userAccount);
        for (SysRoleMngVO tmpMngVO : rtnList) {
            if(!rtn.contains(tmpMngVO.getId())) {
                rtn.add(tmpMngVO.getId());
            }
        }
        return rtn;
    }

    public List<OrgSetMngVO> findOrgSetByRoleids(Set<String>  roleids, String menuId) {
        return getBaseMngDao().findOrgSetByRoleids(roleids,menuId);
    }

    public List<HashMap> findAllPrivList(String cpyCode, String userAccount) {
        return getBaseMngDao().findAllPrivList(cpyCode, userAccount);
    }

}
