package com.yhy.admin.service;

import com.yhy.admin.dao.UserCpyDao;
import com.yhy.admin.vo.UserCpyVO;
import com.yhy.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 下午4:35 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserCpyService extends BaseService<UserCpyVO> {

    @Autowired
    private UserCpyDao userCpyDao;

    @Override
    protected UserCpyDao getDao() {
        return userCpyDao;
    }

    @Override
    protected void preInsert(UserCpyVO entity) {
        super.preInsert(entity);
    }

    public int deleteByUserAccount(String userAccount) {
        return getDao().deleteByUserAccount(userAccount);
    }

    public List<UserCpyVO> findUserCpyByUserAccount(String userAccount) {
        return getDao().findUserCpyByUserAccount(userAccount);
    }

}
