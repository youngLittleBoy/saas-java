package com.yhy.admin.service;

import com.yhy.admin.dao.SysRoleDao;
import com.yhy.admin.vo.SysRoleVO;
import com.yhy.common.service.BaseMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-1 下午5:09 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleService extends BaseMainService<SysRoleVO> {


    @Autowired
    private SysRoleDao baseDao;

    @Override
    protected SysRoleDao getDao() {
        return baseDao;
    }


}
