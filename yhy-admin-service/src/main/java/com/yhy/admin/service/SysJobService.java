package com.yhy.admin.service;

import com.yhy.admin.dao.SysJobDao;
import com.yhy.admin.vo.SysJobVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.utils.YhyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午2:07 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class SysJobService extends BaseMainService<SysJobVO> {

	@Autowired
	private SysJobDao sysJobDao;

	@Override
	protected SysJobDao getDao() {
		return sysJobDao;
	}

	@Override
	protected void preInsert(SysJobVO entity) {
		super.preInsert(entity);
		if(StringUtils.isBlank(entity.getJobCode())) {
			entity.setJobCode(YhyUtils.genSysCode(""));
		}
	}

}
