package com.yhy.admin.vo.mng;

import com.yhy.common.dto.BaseMngVO;
/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午10:16 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:27 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public class CpyMngVO extends BaseMngVO {

    private String cpyName;
    private String cpyLongName;
    private String cpyType;
    private String cpyCode;
    private String cerCode;
    private String ownerApp;
    private String cpyStatus;

    private byte[] img; //图片导出测试


    public CpyMngVO() {
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public String getCpyName() {
        return cpyName;
    }

    public void setCpyName(String cpyName) {
        this.cpyName = cpyName;
    }

    public String getCpyLongName() {
        return cpyLongName;
    }

    public void setCpyLongName(String cpyLongName) {
        this.cpyLongName = cpyLongName;
    }

    public String getCpyType() {
        return cpyType;
    }

    public void setCpyType(String cpyType) {
        this.cpyType = cpyType;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }

    public String getCerCode() {
        return cerCode;
    }

    public void setCerCode(String cerCode) {
        this.cerCode = cerCode;
    }

    public String getOwnerApp() {
        return ownerApp;
    }

    public void setOwnerApp(String ownerApp) {
        this.ownerApp = ownerApp;
    }

    public String getCpyStatus() {
        return cpyStatus;
    }

    public void setCpyStatus(String cpyStatus) {
        this.cpyStatus = cpyStatus;
    }
}
