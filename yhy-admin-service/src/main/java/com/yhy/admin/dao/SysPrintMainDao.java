package com.yhy.admin.dao;

import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import com.yhy.common.vo.SysPrintMainVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 下午4:52 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysPrintMainDao")
public interface SysPrintMainDao  extends BaseMainDao<SysPrintMainVO> {

}
