package com.yhy.admin.dao;

import com.yhy.admin.dto.DataDictDTO;
import com.yhy.admin.vo.mng.DataDictMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午3:05 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

@Mapper
@Component(value = "dataDictMngDao")
public interface DataDictMngDao extends BaseMngDao<DataDictDTO,DataDictMngVO> {


}
