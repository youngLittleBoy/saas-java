package com.yhy.form.service;

import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.utils.YhyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yhy.form.dao.FormApplyMainDao;
import com.yhy.form.vo.FormApplyMainVO;
import com.yhy.common.service.BaseService;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-17 上午10:47 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FormApplyMainService extends BaseMainService<FormApplyMainVO> {

	@Autowired
	private  FormApplyMainDao formApplyMainDao;

	@Override
	protected FormApplyMainDao getDao() {
		return formApplyMainDao;
	}

	@Override
	protected void preInsert(FormApplyMainVO entity) {
		super.preInsert(entity);
		if(StringUtils.isBlank(entity.getApplyCode())) {
			entity.setApplyCode(YhyUtils.genSysCode(BusModuleType.MD_FORM_APPLY_MNG.getVal()));
		}
	}
}
