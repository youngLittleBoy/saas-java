package com.yhy.common.service;

import com.yhy.common.constants.BusDirection;
import com.yhy.common.constants.BusType;
import com.yhy.common.dao.BusCommonStateHistoryDao;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.BusCommonState;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-7 下午6:56 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class BusCommonStateHistoryService extends BaseService<BusCommonState> {

    @Autowired
    private BusCommonStateHistoryDao busCommonStateHistoryDao;

    @Override
    protected BusCommonStateHistoryDao getDao() {
        return busCommonStateHistoryDao;
    }

    @Override
    public int deleteById(BusCommonState entity) {
        return super.deleteById(entity);
    }

    @Override
    protected void preInsert(BusCommonState entity) {
        if(StringUtils.isBlank(entity.getId())) {
            entity.setId(YhyUtils.genId(entity.getBusModule()));
        }
        super.preInsert(entity);
        if(entity.getVersion() == null) {
            entity.setVersion(1);
        }
        if (StringUtils.isBlank(entity.getEnableFlag())) {
            entity.setEnableFlag("Y");
        }
        if (StringUtils.isBlank(entity.getHistoryFlag())) {
            entity.setHistoryFlag("N");
        }
        if (entity.getBusType() == null) {
            entity.setBusType(BusType.ONE.getVal());
        }
        if (entity.getBusDate() == null) {
            entity.setBusDate(YhyUtils.getCurDateTime());
        }
        if (entity.getBusDirection() == null) {
            entity.setBusDirection(BusDirection.SEND.getVal());
        }
        if (StringUtils.isBlank(entity.getSysGenCode())) {
            entity.setSysGenCode(YhyUtils.genSysCode(entity.getBusModule()));
        }
        if (StringUtils.isBlank(entity.getRelNum())) {
            entity.setRelNum(YhyUtils.generateUUID());
        }
    }

    public BusCommonState findByBusModuleAndId(String busModule, String id) {
        return getDao().findByBusModuleAndId(busModule, id);
    }

}
