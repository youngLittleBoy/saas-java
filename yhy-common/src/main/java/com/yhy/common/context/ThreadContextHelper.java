package com.yhy.common.context;

/*
* 线程上下文帮助类
 */
public class ThreadContextHelper {

    /**
     * 上下文线程变量
     */
    private final static ThreadLocal<ThreadContextInfo> threadContext = new InheritableThreadLocal<ThreadContextInfo>() {
        @Override
        protected ThreadContextInfo initialValue() {
            return new ThreadContextInfo();
        }
    };

    @SuppressWarnings("unchecked")
    public static <T> T getParameter(String name) {
        return (T)threadContext.get().getParameter(name);
    }

    public static void setParameter(String name, Object value) {
        threadContext.get().setParameter(name, value);
    }

    @SuppressWarnings("unchecked")
    public static <T> T remove(String name) {
        return (T)threadContext.get().removeParameter(name);
    }

    /**
     * <br>
     * <b>功能：</b>释放上下文信息<br>
     * <b>作者：</b>yanghuiyuan<br>
     * <b>日期：</b> 2019-11-28 <br>
     */
    public static void releaseContext() {
        threadContext.remove();
    }

}
