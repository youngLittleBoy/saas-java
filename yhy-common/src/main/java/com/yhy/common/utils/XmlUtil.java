package com.yhy.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.*;

import java.util.List;

public class XmlUtil {

    public static void main(String[] args) throws Exception {
        String xml2 = "<?xml version=\"1.0\" encoding=\"gbk\"?>\n" +
                "<business comment=\"税种税目信息查询\" id=\"SZSMCX\">\n" +
                "<body yylxdm=\"1\">\n" +
                "<output>\n" +
                "<fplxdm>发票类型代码</fplxdm>\n" +
                "<szsmxx count=\"2\">\n" +
                "<group xh=\"1\">\n" +
                "<szsmsyh>税种税目索引号</szsmsyh>\n" +
                "<szsmdm>税种税目代码</szsmdm>\n" +
                "<sl>税率</sl>\n" +
                "<hsbz>含税标志</hsbz>\n" +
                "<szmc>税种名称</szmc>\n" +
                "<smmc>税目名称</smmc>\n" +
                "</group>\n" +
                "<group xh=\"2\">\n" +
                "<szsmsyh>税种税目索引号</szsmsyh>\n" +
                "<szsmdm>税种税目代码</szsmdm>\n" +
                "<sl>税率</sl>\n" +
                "<hsbz>含税标志</hsbz>\n" +
                "<szmc>税种名称</szmc>\n" +
                "<smmc>税目dd名称</smmc>\n" +
                "</group>\n" +
                "</szsmxx>\n" +
                "<returncode>0</returncode>\n" +
                "<returnmsg>成功</returnmsg>\n" +
                "</output>\n" +
                "</body>\n" +
                "</business>\n";
        System.out.println("xml2Json:"+xml2Json(xml2).toJSONString());

    }
    /**
     * xml转json
     * @param xmlStr
     * @return
     * @throws DocumentException
     */
    public static JSONObject xml2Json(String xmlStr) {
        Document doc= null;
        try {
            doc = DocumentHelper.parseText(xmlStr);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        JSONObject json=new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        return json;
    }

    /**
     * xml转json
     * @param element
     * @param json
     */
    public static void dom4j2Json(Element element, JSONObject json){
        //如果是属性
        for(Object o:element.attributes()){
            Attribute attr=(Attribute)o;
            if(!isEmpty(attr.getValue())){
                json.put("@"+attr.getName(), attr.getValue());
            }
        }
        List<Element> chdEl=element.elements();
        if(chdEl.isEmpty()&&!isEmpty(element.getText())){//如果没有子元素,只有一个值
            json.put(element.getName(), element.getText());
        }

        for(Element e:chdEl){//有子元素
            if(!e.elements().isEmpty()){//子元素也有子元素
                JSONObject chdjson=new JSONObject();
                dom4j2Json(e,chdjson);
                Object o=json.get(e.getName());
                if(o!=null){
                    JSONArray jsona=null;
                    if(o instanceof JSONObject){//如果此元素已存在,则转为jsonArray
                        JSONObject jsono=(JSONObject)o;
                        json.remove(e.getName());
                        jsona=new JSONArray();
                        jsona.add(jsono);
                        jsona.add(chdjson);
                    }
                    if(o instanceof JSONArray){
                        jsona=(JSONArray)o;
                        jsona.add(chdjson);
                    }
                    json.put(e.getName(), jsona);
                }else{
                    if(!chdjson.isEmpty()){
                        json.put(e.getName(), chdjson);
                    }
                }


            }else{//子元素没有子元素
                for(Object o:element.attributes()){
                    Attribute attr=(Attribute)o;
                    if(!isEmpty(attr.getValue())){
                        json.put("@"+attr.getName(), attr.getValue());
                    }
                }
                if(!e.getText().isEmpty()){
                    json.put(e.getName(), e.getText());
                }
            }
        }
    }

    public static boolean isEmpty(String str) {
        if (str == null || str.trim().isEmpty() || "null".equals(str)) {
            return true;
        }
        return false;
    }
}
